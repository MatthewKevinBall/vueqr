namespace :symfony do
  desc "Runs database migrations"
  task :db_migrate do
    invoke 'symfony:console', 'doctrine:migrations:migrate', '--no-interaction'
  end
end
