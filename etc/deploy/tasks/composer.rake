# Composer
namespace :composer do
  # Install composer if it does not exist
  before :install_executable, :map_composer do
    SSHKit.config.command_map[:composer] = "php #{shared_path.join("composer.phar")}"
  end
end
