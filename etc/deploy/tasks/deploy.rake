namespace :deploy do
  desc "Sets the permissions according to the defined permission method"
  task :update_permissions do
    invoke "deploy:set_permissions:#{fetch(:permission_method).to_s}"
  end

  desc "Builds frontend assets"
  task :frontend_build do
    on release_roles(:web) do
      within release_path do
        execute 'yarn', 'install'
        execute 'yarn', 'build'
      end
    end
  end


  desc 'Restarts PHP FPM'
  task :restart_php_fpm do
    on release_roles :app do
      execute :sudo, 'systemctl', 'restart', 'php7.2-fpm.service'
    end
  end
end
