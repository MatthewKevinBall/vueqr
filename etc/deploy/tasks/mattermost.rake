namespace :mattermost do
  desc "Notify MatterMost satellitemedia #deploys channel"
  task :notify do
    run_locally do
      execute 'curl', '-X', 'POST', '--silent', '--data-urlencode', "'payload={\"text\": \"Deployed #{fetch(:current_revision)} to #{fetch(:application)} (#{fetch(:stage)})\", \"channel\": \"#deploys\", \"username\": \"monkey-bot\", \"icon_emoji\": \":monkey_face:\"}'", 'https://mm.satellite.co.nz/hooks/ujzw4w17ijf4mbc1d9wzwfnj9o'
    end
  end
end
