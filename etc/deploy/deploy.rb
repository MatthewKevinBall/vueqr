# Deployment Configuration
# ========================
#
# For more information on the default capistrano/symfony configuration
# see: https://github.com/capistrano/symfony/blob/2.0.0-alfa2/README.md

# config valid for current version and patch releases of Capistrano
lock '~> 3.11.0'

# Settings
# ========

# Use Composer version 1.
set :composer_version, '1.10.23'

set :repo_url, 'git@git.satellite.co.nz:satellite/house-of-travel/travel-documentation.git'
set :permission_method, :chmod
set :keep_releases, 3

# Linked Files

# append :linked_dirs, 'public/uploads'

# Deployment Stage Defaults
# -------------------------

# Deployment Server: mixandmatch (13.237.92.158)
server '13.237.92.158', user: 'satellite', roles: ['app', 'db', 'web']

# Hooks
# =====

# Deploy Hooks
namespace :deploy do

  after :starting, 'composer:install_executable'
#   after :updated, 'symfony:db_migrate'

  after :published, 'deploy:restart_php_fpm'

  namespace :symlink do
    # Build the frontend assets before the app is symlinked, the assets
    # should not depend on the shared files and folders.
    before :shared, "deploy:frontend_build"
  end
end
